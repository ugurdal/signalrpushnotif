﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WorkFromHome.App_Start;

namespace WorkFromHome.Helper
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        WFHContext context = new WFHContext(); // my entity  
        private readonly string[] allowedroles;
        public CustomAuthorize(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            foreach (var role in allowedroles)
            {
                var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
                // Get the claims values
                try
                {
                    String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
                    var loginUser = context.Users.Where(x => x.Username == userName).SingleOrDefault();
                    int roleId = context.Roles.SingleOrDefault(x => x.RoleName == role).RoleID;
                    var user = context.Users.Where(m => m.RoleID == roleId && m.Username == userName).ToList();
                    if (user.Count() > 0)
                    {
                        authorize = true; /* return true if Entity has current user(active) with specific role */
                    }

                }
                catch (Exception e)
                {
                    return false;
                }

            }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error.cshtml"
            };
        }


    }
}