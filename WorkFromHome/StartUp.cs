﻿using Microsoft.Owin;
using Owin;
using WorkFromHome.Models;

[assembly: OwinStartupAttribute(typeof(WorkFromHome.App_Start.StartUp))]
namespace WorkFromHome.App_Start
{
    public partial class StartUp
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
            
        }


    }
}