﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WorkFromHome.App_Start;
using WorkFromHome.Helper;
using WorkFromHome.Models;

namespace WorkFromHome.Controllers
{
    [CustomAuthorize("Administration")]
    public class TaskDetailsController : Controller
    {
        private WFHContext db = new WFHContext();

        // GET: TaskDetails
        public ActionResult Index()
        {
            var taskDetails = db.TaskDetails.Include(t => t.TaskBy).Include(t => t.TaskTo);
            return View(taskDetails.ToList());
        }

        
        public ActionResult Create()
        {
            ViewBag.TaskByID = new SelectList(db.Users, "UserID", "FullName");
            ViewBag.TaskToID = new SelectList(db.Users.Where(x=>x.RoleID !=1), "UserID", "FullName");
            return View();
        }

      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TaskID,Date,Task,TaskResponse,TaskByID,TaskToID")] TaskDetail taskDetail)
        {
            var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;
            String userName = identity.Claims.Where(c => c.Type == ClaimTypes.Name)
                                                      .Select(c => c.Value).SingleOrDefault();
            var loginUser = db.Users.Where(x => x.Username == userName).SingleOrDefault();
            if (ModelState.IsValid)
            {
                taskDetail.TaskByID = loginUser.UserID;
                taskDetail.TaskResponse = "";
                db.TaskDetails.Add(taskDetail);
                db.SaveChanges();
                return RedirectToAction("Tasks", "Admin");
            }

            ViewBag.TaskByID = new SelectList(db.Users, "UserID", "FullName", taskDetail.TaskByID);
            ViewBag.TaskToID = new SelectList(db.Users, "UserID", "FullName", taskDetail.TaskToID);
            return View(taskDetail);
        }

     

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
